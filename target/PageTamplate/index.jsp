<!DOCTYPE html>
<html lang="en">
<head>
<title>Student</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
/* Set height of the grid so .sidenav can be 100% (adjust if needed) */
.row.content {
	height: 1500px
}

/* Set gray background color and 100% height */
.sidenav {
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
header {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}
</style>
</head>
<body>
	<header class="container-fluid">
		<p>SoftStudent.Service</p>
	</header>
	<div class="container-fluid">
		<div class="row content">
			<div class="col-sm-3 sidenav">
				<h4>Student_Nav_Bar</h4>
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="#uploadDiv">CreateDB</a></li>
					<li class="active"><a href="#donwloadDiv">Download JSON
							file</a></li>
					<li class="active"><a href="#AddStudent">Add Student</a></li>
					<li class="active"><a href="#AddLesson">Add Lesson</a></li>
					<li class="active"><a href="#Students">Students</a></li>
					<li class="active"><a href="#Lessons">Lessons</a></li>
				</ul>
				<br>
				<div class="input-group">
					<input type="text" class="form-control"
						placeholder="Search Student.."> <span
						class="input-group-btn">
						<button class="btn btn-default" type="button">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</div>

			<div id="uploadDiv" class="col-sm-9">
				<h4>
					<small>UPLOAD</small>
				</h4>
				<form action="upload" method="post" enctype="multipart/form-data">
					<input type="file" name="file" />



					<button id="upload" type="submit" name="Upload">Upload</button>


				</form>
			</div>
			<div id="downloadDiv" class="col-sm-9">
				<h4>
					<small>DOWNLOAD</small>
				</h4>
				<form action="download" method="get">

					<button id="download" type="submit" name="Download">Download</button>


				</form>
			</div>
			<div id="AddStudent" class="col-sm-9">
				<h4>
					<small>ADD STUDENT</small>
				</h4>
				<form action="PersonServlet" method="post">
					<input type="text" value="addFirstName" name="addFirstName" /> <input
						type="text" value="addLastName" name="addLastName" />
					<button id="addSudent" type="submit" name="addStudent">Add
						Student</button>
				</form>
			</div>
			<div id="AddLesson" class="col-sm-9">
				<h4>
					<small>ADD LESSON</small>
				</h4>
				<form action="AddLesson" method="post">
					<input type="text" value="addSubject" name="addSubject" /> Enter
					date ("yyyy_MM_DD"). <input type="text" value="addYear"
						name="addYear" /> <input type="text" value="addMonth"
						name="addMonth" /> <input type="text" value="addDay"
						name="addDay" /> <input type="text" value="addMark"
						name="addMark" /> Enter existing Student ID.(Should work with
					select and listener to choose existing student). <input type="text"
						value="addStudentID" name="addStudentID" />
					<button id="addLesson" type="submit" name="addLesson">Add
						Lesson</button>
				</form>
			</div>
			<div id="Students" class="col-sm-9">
				<h4>
					<small>STUDENTS</small>
				</h4>
				<%@ page import="java.sql.ResultSet"%>
				<%@ page import="java.sql.Statement"%>
				<%@ page import="java.sql.Connection"%>
				<%@ page import="java.sql.DriverManager"%>
				<form action="StudentsServlet" method="post">
					<table>
						<tr>
							<td>First Name</td>
							<td>Last Name</td>
						</tr>
						<%
							try {
								String JDBC_DRIVER = "org.postgresql.Driver";
								String DB_URL = "jdbc:postgresql://localhost/";
								String URL = "jdbc:postgresql://localhost/studentservicedb";
								String USER = "postgres";
								String PASS = "kid553141";
								String query = "SELECT from students";
								Connection conn = DriverManager.getConnection(URL, USER, PASS);
								Statement stmt = conn.createStatement();
								ResultSet rs = stmt.executeQuery(query);
								while (rs.next()) {
						%>

						<tr>
							<td>
								<%
									rs.getString("firstname");
								%>
							</td>
														<td>
								<%
									rs.getString("lastname");
								%>
							</td>
						</tr>
				
						<%
							}
						%>
					</table>
					<%
						rs.close();
							stmt.close();
							conn.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					%>

				</form>
			</div>
			<div id="Lessons" class="col-sm-9">
				<h4>
					<small>LESSONS</small>
				</h4>
				<form action="LessonsServlet" method="post">
					<table>
						<tr>
							<td>First Name</td>
							<td>Last Name</td>
							<td>Subject</td>
							<td>Date</td>
							<td>Mark</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>


</body>
</html>
