package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import studentObject.Lesson;
import studentObject.Student;





public class DBConnection {

	static List<Lesson> lessonList = null;
	static Connection conn = null;
	static Statement stmt = null;
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	// __________________________________________________________________________________________________________________

	public static void connectionDB() {
		// STEP 2: Register JDBC driver
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// STEP 3: Open a connection
		System.out.println("Connecting to database...");
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void closeDB() {
		try {
			conn.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	// __________________________________________________________________________________________________________________

	public static void creationDB() {

		try {
			

			// STEP 2: Register JDBC driver
			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection

			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query

			stmt = conn.createStatement();
			String sql = "DROP DATABASE IF EXISTS StudentServiceDB";
			stmt.executeUpdate(sql);
			sql = "CREATE DATABASE StudentServiceDB";
			stmt.executeUpdate(sql);

			stmt.close();
			conn.close();

			conn = DriverManager.getConnection(URL, USER, PASS);
			stmt = conn.createStatement();

			String sql1 = "CREATE TABLE students " + " (ID SERIAL PRIMARY KEY, " + " firstname VARCHAR(255) not NULL, "
					+ " lastname VARCHAR(255) not NULL)";

			stmt.executeUpdate(sql1);

			String sql2 = " CREATE TABLE lessons " + " (lessonID SERIAL PRIMARY KEY, "
					+ " subjectName VARCHAR(255) not NULL, " + " localDate DATE, " + " mark INTEGER, "
					+ "ID INTEGER not NULL)";

			stmt.executeUpdate(sql2);

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		}

		catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		/*
		  finally { // finally block used to close resources try { if (stmt !=
		  null) stmt.close(); } catch (SQLException se2) { } // nothing we can
		  do try { if (conn != null) conn.close(); } catch (SQLException se) {
		  se.printStackTrace(); } // end finally try } // end try
		 */

	}

	// __________________________________________________________________________________________________________________

	public static void insertToDb(List<Student> studList) {
		ResultSet rs = null;
		
		try {
			for (Student st : studList) {
				lessonList = st.getLessonsList();
				String sql3 = "INSERT INTO 	students (firstname, lastname) " + "VALUES ('" + st.getFirstName() + "' , '"
						+ st.getLastName() + "')";
				stmt.execute(sql3);
				String lastID = "SELECT currval(pg_get_serial_sequence('students','id'))";
				rs = stmt.executeQuery(lastID);
				rs.next();
				int stud_id;
				stud_id = rs.getInt(1);
				for (Lesson l : st.getLessonsList()) {
					String sql4 = "INSERT INTO 	lessons (subjectname, localdate, mark, id)" + "VALUES ('"
							+ l.getSubjectName() + "', '" + l.getLessonDate() + "', " + l.getMark() + ", " + stud_id
							+ ")";
					stmt.execute(sql4);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		  finally { 
			  // finally block used to close resources 
			  try { 
			  if (stmt !=null) 
				  stmt.close(); 
			  } catch (SQLException se2) { 
			  } 
		// nothing we can do 
		try { if (conn != null) 
			conn.close();
		} catch (SQLException se) {
		  se.printStackTrace(); 
		  } 
		// end finally try 
		} 
		// end try

	}
}
