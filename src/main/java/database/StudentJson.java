package database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import studentObject.Student;

public class StudentJson {

	public static void creationOfJsonFile(List<Student> studObject) {

		ObjectMapper mapper = new ObjectMapper();

		try {

			mapper.writerWithDefaultPrettyPrinter().writeValue(new File("E:\\jeeworkspace\\PageTamplate\\src\\main\\resources\\StudentObject1.json"), studObject);


		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static String creationOfJsonFileFromDB(List<Student> studObject) {

		ObjectMapper mapper = new ObjectMapper();
		String data = "";

		try {
			

			data = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(studObject);
			

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;

	}

	public static void deserializeStudList() {
		List<Student> data = null;
		try {
			ObjectMapper mapper = new ObjectMapper();

			data = mapper.readValue(new File("E:\\StudentObject.json"), new TypeReference<List<Student>>(){});
		

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	
	public static List <Student> deserializeStudentList(String fileContent) {
		List<Student> studList = null;
		try {
			ObjectMapper mapper = new ObjectMapper();

			studList = mapper.readValue(fileContent, new TypeReference<List<Student>>(){});
		

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return studList;
	}

}
