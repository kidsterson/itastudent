package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import studentObject.Lesson;
import studentObject.Student;

public class CreateObjectFromDB {
	static List<Lesson> lessonList = new ArrayList<Lesson>();;
	static List<Student> studentList = new ArrayList<Student>();
	static Connection conn = null;
	static Statement stmt = null;

	static ResultSet rs = null;
	static ResultSet rs1 = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	public static List<Student> createObject() {

		try {
			// STEP 2: Register JDBC driver
			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(URL, USER, PASS);
			System.out.println("Connected database successfully...");

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql1 = "SELECT ID, firstName, lastName FROM students";
			rs1 = stmt.executeQuery(sql1);
			while (rs1.next()) {
				int studID = rs1.getInt("id");
				String firstN = rs1.getString("firstname");
				String lastN = rs1.getString("lastname");

				Student stud1 = new Student();
				stud1.setID(studID);
				stud1.setFirstName(firstN);
				stud1.setLastName(lastN);
				studentList.add(stud1);
			}
			rs1.close();

			for (Student stud : studentList) {
				String sql = "SELECT lessonID, subjectName, localDate, mark FROM lessons " + "WHERE Id = "
						+ stud.getID() + "  ";
				rs = stmt.executeQuery(sql);
				lessonList.clear();
				// STEP 5: Extract data from result set
				while (rs.next()) {
					// Retrieve by column name
					Lesson lesson = new Lesson();
					int lessonID = rs.getInt("lessonid");
					lesson.setLessonID(lessonID);
					String subjectName = rs.getString("subjectname");
					lesson.setSubjectName(subjectName);
					LocalDate date = rs.getObject("localdate", LocalDate.class);
					lesson.setLessonDate(date);
					int mark = rs.getInt("mark");
					lesson.setMark(mark);
					lessonList.add(lesson);

					// Display values
					System.out.print("ID: " + lessonID);
					System.out.print(", subject: " + subjectName);
					System.out.print(", lesson Date: " + date);
					System.out.println(", mark: " + mark);
				}
				stud.setLessonsList(lessonList);

			}

			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
			// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			// end finally try
		}
		return studentList;

	}

}
