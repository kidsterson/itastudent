package Selvlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentsServlet
 */
public class StudentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Connection conn = null;
	static Statement stmt = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StudentsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {

			// STEP 2: Register JDBC driver

			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection

			conn = DriverManager.getConnection(URL, USER, PASS);

			String query="SELECT from students";
		
			stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(query);
		while(rs.next())
		{

		  //  out.println(rs.getInt("userID")); 
		   // out.println(rs.getDate("dob")); 
		   // out.println(rs.getString("gender"));
		   // out.println(rs.getString("firstName")); 
		   // out.println(rs.getString("lastName")); 

		 

		}
		
		
		
		rs.close();
		stmt.close();
		conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		}

		catch (Exception e1) {
			// Handle errors for Class.forName
			e1.printStackTrace();
		}

		
		doGet(request, response);
	

}
}
