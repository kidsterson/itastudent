package Selvlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import studentObject.Student;

/**
 * Servlet implementation class PersonServlet
 */
public class PersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Connection conn = null;
	static Statement stmt = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	/**
	 * Default constructor.
	 */
	public PersonServlet() {

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		
		
		

		try {

			// STEP 2: Register JDBC driver

			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection

			conn = DriverManager.getConnection(URL, USER, PASS);
			
			stmt = conn.createStatement();

		    // Prepare the SQL statement to insert, plug in the values
		    PreparedStatement stmt = conn.prepareStatement("INSERT INTO students (firstname, lastname) VALUES (?, ?)");
		    stmt.setString(1, firstName);
		    stmt.setString(2, lastName);
	

		    // Execute the insert
		    stmt.executeUpdate();
		    conn.close();
		    Student st = new Student();
		    st.setFirstName(firstName);
		    st.setLastName(lastName);
		    ObjectMapper ob = new ObjectMapper();
		    response.getWriter().write(ob.writerWithDefaultPrettyPrinter().writeValueAsString(st));

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		}

		catch (Exception e1) {
			// Handle errors for Class.forName
			e1.printStackTrace();
		}
	}

}
