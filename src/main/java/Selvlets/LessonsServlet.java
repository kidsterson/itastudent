package Selvlets;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import studentObject.Lesson;
import studentObject.Student;

/**
 * Servlet implementation class LessonsServlet
 */
public class LessonsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	static Connection conn = null;
	static Statement stmt = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LessonsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String data = "";
		String query = request.getQueryString();
		String [] param = query.split("&");
		
		String firstname = "";
		String lastname = "";
		
		for (String str : param) {
			String k = str.split("=")[0];
			String v = str.split("=")[1];
			if (k.equals("firstname")) {
				firstname = v;
			} else if (k.equals("lastname")) {
				lastname = v;
			}
			

			
		}
		
		Student stud1 = new Student();
		stud1.setFirstName(firstname);
		stud1.setLastName(lastname);

		try {

			// STEP 2: Register JDBC driver

			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection

			conn = DriverManager.getConnection(URL, USER, PASS);
			
			stmt = conn.createStatement();

		    // Prepare the SQL statement to insert, plug in the values
		    PreparedStatement stmt = conn.prepareStatement("SELECT s.firstname, s.lastname, l.subjectname, l.localdate, l.mark FROM students AS s INNER JOIN lessons AS l ON s.id = l.id WHERE s.firstname = '"+stud1.getFirstName()+"' AND s.lastname = '"+stud1.getLastName()+"'");
		    
		    
		    
		    // Execute the insert
		    ResultSet rs = stmt.executeQuery();
		    
		    while(rs.next())
			{
		    	
		    	Lesson less = new Lesson();
		    	less.setSubjectName(rs.getString("subjectname"));
		    	less.setLessonDate(LocalDate.parse(rs.getString("localdate")));
		    	less.setMark(rs.getInt("mark"));
		    	
		    	stud1.getLessonsList().add(less);

			}
		    
		    conn.close();
		    
		    ObjectMapper mapper = new ObjectMapper();

				data = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(stud1);


		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		}

		catch (Exception e1) {
			// Handle errors for Class.forName
			e1.printStackTrace();
		}
		
		
		
		
		//String testData = "{\"firstName\" : \"Sasha\",\"lastName\" : \"Yushhenko\",\"lessonsList\" : [ {\"subjectName\" : \"Math\",\"lessonDate\" : \"2010-06-07\",\"mark\" : 5}, {\"subjectName\" : \"Math\", \"lessonDate\" : \"2010-06-07\", \"mark\" : 5 }]}";
		response.getWriter().append(data);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		doGet(request, response);
	}

}
