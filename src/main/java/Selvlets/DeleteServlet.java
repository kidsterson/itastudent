package Selvlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import studentObject.Lesson;

/**
 * Servlet implementation class DeleteServlet
 */
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static Connection conn = null;
	static Statement stmt = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost/";
	static final String URL = "jdbc:postgresql://localhost/studentservicedb";

	// Database credentials
	static final String USER = "postgres";
	static final String PASS = "kid553141";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");

		try {

			// STEP 2: Register JDBC driver

			Class.forName("org.postgresql.Driver");

			// STEP 3: Open a connection

			conn = DriverManager.getConnection(URL, USER, PASS);

			stmt = conn.createStatement();

			// Prepare the SQL statement to insert, plug in the values
			PreparedStatement stmt = conn.prepareStatement(
					"DELETE FROM lessons l USING students s WHERE s.id = l.id AND s.firstname = '" + firstname
							+ "' AND s.lastname = '" + lastname + "'; DELETE FROM students s WHERE s.firstname = '"
							+ firstname + "' AND s.lastname = '" + lastname + "'");

			stmt.executeUpdate();
			conn.close();

			response.getWriter().append("It's been deleted");

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		}

		catch (Exception e1) {
			// Handle errors for Class.forName
			e1.printStackTrace();
		}

	}

}
