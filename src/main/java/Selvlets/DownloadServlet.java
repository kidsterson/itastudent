package Selvlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.CreateObjectFromDB;
import database.StudentJson;
import studentObject.Student;
/**
 * Servlet implementation class UploalServlet
 */
@WebServlet("/download")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List <Student> stud;
       
    /**
     * Default constructor. 
     */
    public DownloadServlet() {
        super();
       
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// reads input file from an absolute path
		
			if (stud != null){
				stud.clear();
			}
		stud = CreateObjectFromDB.createObject();
		String content = StudentJson.creationOfJsonFileFromDB(stud);
		
       // FileInputStream inStream = new FileInputStream(downloadFile);
         
        // if you want to use a relative path to context root:
        // String relativePath = getServletContext().getRealPath("");

         
        // gets MIME type of the file
        String mimeType = "application/json";
        
        // modifies response
        response.setContentType(mimeType);
        //response.setContentLength((int) content.length());
         
        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"downloaded.json\"");
        response.setHeader(headerKey, headerValue);
        
        response.getWriter().write(content);
         
        // obtains response's output stream
        /*OutputStream outStream = response.getOutputStream();
         
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
         
        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
         
        inStream.close();
        outStream.close();   */   
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
