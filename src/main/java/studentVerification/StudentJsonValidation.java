package studentVerification;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.IOUtils;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

public class StudentJsonValidation {

	public static boolean validation() throws IOException, ProcessingException {
		
			File schemaFile = new File("D:\\StudentObjectSchema.json");
			File jsonFile = new File("D:\\StudentObject.json");

			return ValidationUtils.isJsonValid(schemaFile, jsonFile);
	
	}
	
	public static boolean validateStrings(String jsonText) throws IOException, ProcessingException {
		String schema = IOUtils.toString(StudentJsonValidation.class.getClassLoader().getResourceAsStream("StudentObjectSchema.json"));
		boolean valid = ValidationUtils.isJsonValid(schema, jsonText);
		return valid;
		
	}
}
