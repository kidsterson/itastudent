package studentObject;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import database.LocalDateDeserializer;
import database.LocalDateSerializer;

public class Lesson {
	public Lesson() {
		super();
	}

	public Lesson(int lessonID, String subjectName, LocalDate lessonDate, int mark) {
		super();
		this.lessonID = lessonID;
		this.subjectName = subjectName;
		this.lessonDate = lessonDate;
		this.mark = mark;
	}
	@JsonIgnore
	public int getLessonID() {
		return lessonID;
	}

	public void setLessonID(int lessonID) {
		this.lessonID = lessonID;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public LocalDate getLessonDate() {
		return lessonDate;
	}

	public void setLessonDate(LocalDate lessonDate) {
		this.lessonDate = lessonDate;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	private int lessonID;
	private String subjectName;
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate lessonDate;
	private int mark;

}
