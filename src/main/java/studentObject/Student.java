package studentObject;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class Student {
	
	private int ID;
	private String firstName;
	private String lastName;

	public Student() {
		super();
	}

	public Student(int iD, String firstName, String lastName, List<Lesson> lessonsList) {
		super();
		this.ID = iD;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lessonsList = lessonsList;
	}
	@JsonIgnore
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		this.ID = iD;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firsName) {
		this.firstName = firsName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Lesson> getLessonsList() {
		return lessonsList;
	}

	public void setLessonsList(List<Lesson> lessonsList) {
		this.lessonsList = lessonsList;
	}

	private List<Lesson> lessonsList = new ArrayList<Lesson>();
}
