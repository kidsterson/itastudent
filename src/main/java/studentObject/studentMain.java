package studentObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import database.StudentJson;

public class studentMain {

	public static void main(String[] args) {

Lesson firstStudentLesson1 = new Lesson(1, "Math", LocalDate.of(2010, 06, 07), 5);
Lesson firstStudentLesson2 = new Lesson(2, "Math", LocalDate.of(2010, 06, 07), 5);
Lesson firstStudentLesson3 = new Lesson(3, "Math", LocalDate.of(2010, 06, 07), 5);
List <Lesson> firstStudentLessonList = new ArrayList<Lesson>();
firstStudentLessonList.add(firstStudentLesson1);
firstStudentLessonList.add(firstStudentLesson2);
firstStudentLessonList.add(firstStudentLesson3);
Student firstStudent = new Student(1, "Sasha", "Yushhenko", firstStudentLessonList);

Lesson secondStudentLesson1 = new Lesson(4, "Math", LocalDate.of(2010, 06, 07), 4);
Lesson secondStudentLesson2 = new Lesson(5, "Math", LocalDate.of(2010, 06, 07), 4);
Lesson secondStudentLesson3 = new Lesson(6, "Math", LocalDate.of(2010, 06, 07), 4);
List <Lesson> secondStudentLessonList = new ArrayList<Lesson>();
secondStudentLessonList.add(secondStudentLesson1);
secondStudentLessonList.add(secondStudentLesson2);
secondStudentLessonList.add(secondStudentLesson3);
Student secondStudent = new Student(2, "Petya", "Petrov", secondStudentLessonList);

Lesson thirdStudentLesson1 = new Lesson(7, "Math", LocalDate.of(2010, 06, 07), 3);
Lesson thirdStudentLesson2 = new Lesson(8, "Math", LocalDate.of(2010, 06, 07), 3);
Lesson thirdStudentLesson3 = new Lesson(9, "Math", LocalDate.of(2010, 06, 07), 3);
List <Lesson> thirdStudentLessonList = new ArrayList<Lesson>();
thirdStudentLessonList.add(thirdStudentLesson1);
thirdStudentLessonList.add(thirdStudentLesson2);
thirdStudentLessonList.add(thirdStudentLesson3);
Student thirdStudent = new Student(3, "Vasia", "Yushhenko", thirdStudentLessonList);

StudentJson sj = new StudentJson();
List <Student> studList = new ArrayList<Student>();
studList.add(firstStudent);
studList.add(secondStudent);
studList.add(thirdStudent);

sj.creationOfJsonFile(studList);
sj.deserializeStudList();

 
	}

}
