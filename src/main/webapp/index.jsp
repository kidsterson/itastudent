<!DOCTYPE html>
<html lang="en">
<head>
<title>Student</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
	src="Ajax.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
/* Set height of the grid so .sidenav can be 100% (adjust if needed) */
.row.content {
	height: 1500px
}

/* Set gray background color and 100% height */
.sidenav {
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
header {
	background-color: #555;
	color: white;
	padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 15px;
	}
	.row.content {
		height: auto;
	}
}
</style>
</head>
<body>
	<header class="container-fluid">
		<p>SoftStudent.Service</p>
	</header>
	<div class="container-fluid">
		<div class="row content">
			<div class="col-sm-3 sidenav">
				<h4>Student_Nav_Bar</h4>
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="#uploadDiv">CreateDB</a></li>
					<li class="active"><a href="#donwloadDiv">Download JSON
							file</a></li>
					<li class="active"><a href="#AddStudent">Add Student</a></li>
					<li class="active"><a href="#AddLesson">Add Lesson</a></li>
					<li class="active"><a href="#Students">Students</a></li>
					<li class="active"><a href="#Lessons">Lessons</a></li>
				</ul>
				<br>
			</div>
			<div id="uploadDiv" class="col-sm-9">
				<h4>
					<small>UPLOAD</small>
				</h4>
				<form action="upload" method="post" enctype="multipart/form-data">
					<input type="file" name="file" />
					<br>
					<button id="upload" type="submit" name="Upload">Upload</button>
				</form>
			</div>
			<br>
			<div id="downloadDiv" class="col-sm-9">
				<h4>
					<small>DOWNLOAD</small>
				</h4>
				<form action="download" method="get">
					<button id="download" type="submit" name="Download">Download</button>
				</form>
			</div>
			<br>
			<div id="AddStudent" class="col-sm-9">
				<h4>
					<small>ADD STUDENT</small>
				</h4>
				Enter first name
					<input id="addFirstName" type="text" value="" name="addFirstName" /> 
					<br>
					Enter last name
					<input id="addLastName" type="text" value="" name="addLastName" />
					<br>
					<button id="addSudent" type="button" name="addStudent" onclick="addStudent()">Add Student</button>
			</div>
			<br>
			<div id="AddLesson" class="col-sm-9">
				<h4>
					<small>ADD LESSON</small>
				</h4>
				<form action="AddLesson" method="post">
				Subject name
				     <input id="addSubject" type="text" value="" name="addSubject" />
					 <br>
					 Enter year (yyyy)
					 <input id="year" type="text" value="" name="addYear" /> 
					 <br>
					 Enter month (MM)			
					 <input id="month" type="text" value="" name="addMonth" /> 
					 <br>
					 Enter day (dd)				
					 <input id="day" type="text" value="" name="addDay" /> 
					 <br>
					 Enter mark					
					 <input id="addMark" type="text" value="" name="addMark" /> 
					 <br>
					 Enter existing Student ID 
					 <input id="addStudentID" type="text" value="" name="addStudentID" />
					 <br>
				     <button id="addLesson" type="submit" name="addLesson" onclick="addLesson()">Add Lesson</button>
				</form>
			</div>
			<br>
			<div id="Students" class="col-sm-9">
				<h4>
					<small>STUDENTS</small>
				</h4>
				<%@ page import="java.sql.ResultSet"%>
				<%@ page import="java.sql.Statement"%>
				<%@ page import="java.sql.Connection"%>
				<%@ page import="java.sql.DriverManager"%>
				<form action="StudentsServlet" method="post">
				<br>
					<table id="students_tbl" border="1">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th> </th>
							<th> </th>
						</tr>
						<%
							try {
								String JDBC_DRIVER = "org.postgresql.Driver";
								String DB_URL = "jdbc:postgresql://localhost/";
								String URL = "jdbc:postgresql://localhost/studentservicedb";
								String USER = "postgres";
								String PASS = "kid553141";
								String query = "SELECT * from students";
								Class.forName("org.postgresql.Driver");
								Connection conn = DriverManager.getConnection(URL, USER, PASS);
								Statement stmt = conn.createStatement();
								ResultSet rs = stmt.executeQuery(query);
								while (rs.next()) {
						%>

						<tr>
							<td>
							
								<%
								out.println(rs.getString("firstname"));
								%>
							</td>
														<td>
														
								<%
								out.println(rs.getString("lastname"));
								%>
							</td>
							<td>
							<a href="#" onclick="loadLessons(this.parentElement)"> Show lessons </a>
							</td>
							<td>
							<a href="#" onclick="deleteStudent(this.parentElement)"> Delete </a>
							</td>
						</tr>
				
						<%
							}
						%>
					</table>
					<%
						rs.close();
							stmt.close();
							conn.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					%>

				</form>
			</div>
			<br>
			<div id="Lessons" class="col-sm-9">
				<h4>
					<small>LESSONS</small>
				</h4>
				<br>
				<table id="lessons_tbl" border="1">
					<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Subject</th>
						<th>Date</th>
						<th>Mark</th>
					</tr>
					</thead>
					<tbody id="lessons_tbl_body">
					</tbody>
				</table>
			</div>
		</div>
	</div>


</body>
</html>
