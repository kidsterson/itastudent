/**
 * 
 */
function loadLessons(td){
	var tr = td.parentElement;
	var firstname = tr.cells[0].innerText;
	var lastname = tr.cells[1].innerText;

	$.get("LessonsServlet",
		{
		    firstname: firstname,
			lastname: lastname
		},
		onLessonsLoaded
		);
}

function onLessonsLoaded(data)
{
	var st = JSON.parse(data);
	$("#lessons_tbl_body").empty();
	for (i = 0; i < st.lessonsList.length; i++) {
		$("#lessons_tbl_body").append("<tr><td>"+st.firstName+"</td><td>"+st.lastName+"</td><td>"+st.lessonsList[i].subjectName+"</td><td>"+st.lessonsList[i].lessonDate+"</td><td>"+st.lessonsList[i].mark+"</td></tr>");
	}
}

function deleteStudent(td){
	var tr = td.parentElement;
	var firstname = tr.cells[0].innerText;
	var lastname = tr.cells[1].innerText;

	$.post("DeleteServlet",
		{
		    firstname: firstname,
			lastname: lastname
		},
		onDeleted
		);
	tr.remove();
}

function onDeleted(data) {
	$("#lessons_tbl_body").empty();
}

function addStudent(){
	
	var firstname = $("#addFirstName").val();
	var lastname = $("#addLastName").val();

	$.post("PersonServlet",
		{
		    firstname: firstname,
			lastname: lastname
		},
		onStudentAdded
		);
}

function onStudentAdded(data)
{
	var st = JSON.parse(data);
	$("#students_tbl").append("<tr><td>"+st.firstName+"</td><td>"+st.lastName+"</td><td><a href='#' onclick='loadLessons(this.parentElement)'> Show lessons </a></td><td><a href='#' onclick='deleteStudent(this.parentElement)'> Delete </a></td></tr>");
}
function addLesson(){
	
	var subjectname = $("addSubject").val();
	var year = $("#year").val();
	var month = $("#month").val();
	var day = $("#day").val();
	var mark = $("#addMark").val();
	var id = $("#addStudentID").val();
	var localdate = year + "-" + month + "-" + day;
	

	$.post("AddLesson",
		{
		subjectname: subjectname,
		localdate: localdate,
		mark: mark,
		id: id
		},
		onLessonAdded
		);
}

function onLessonAdded(data)
{
	var st = JSON.parse(data);

}